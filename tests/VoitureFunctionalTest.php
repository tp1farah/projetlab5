<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VoitureFunctionalTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/voiture/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Voiture index');
    }

    public function testNew()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/voiture/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Voiture');

        // Submit the form with valid data
        $form = $crawler->selectButton('Save')->form();
        $form['voiture[serie]'] = '123456';
        $form['voiture[date_mise]'] = '2022-01-01';
        $form['voiture[marche]'] = 'Brand';
        $form['voiture[modele]'] = 'Model';
        $form['voiture[prix_jour]'] = '100';

        $client->submit($form);

        $this->assertResponseRedirects('/voiture/');
        $client->followRedirect();

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', '123456'); // Adjust based on your application's logic
    }

  // public function testShow()
//{
  //  $client = static::createClient();

    //$crawler = $client->request('GET', '/voiture/1'); // Assuming there is a voiture with ID 1

    //$this->assertResponseIsSuccessful();
    //$this->assertSelectorTextContains('h1', 'Voiture details');

    // Add more assertions related to the voiture details if needed
    //$this->assertSelectorTextContains('.voiture-serie', '123456'); // Adjust based on your HTML structure
//}

//public function testEdit()
//{
  //  $client = static::createClient();

    //$crawler = $client->request('GET', '/voiture/1/edit'); // Assuming there is a voiture with ID 1

    //$this->assertResponseIsSuccessful();
    //$this->assertSelectorTextContains('h1', 'Edit Voiture');

    // Submit the form with updated data
    //$form = $crawler->selectButton('Save')->form();
    //$form['voiture[modele]'] = 'Updated Model';

    //$client->submit($form);

    //$this->assertResponseRedirects('/voiture/');
    //$client->followRedirect();

    //$this->assertResponseIsSuccessful();
    //$this->assertSelectorTextContains('.voiture-modele', 'Updated Model'); // Adjust based on your HTML structure
//}

}
