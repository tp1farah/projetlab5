// tests/Controller/LocationFunctionalTest.php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LocationFunctionalTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/location/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Location Index');
    }

    public function testNew()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/location/new');

        $this->assertResponseIsSuccessful();

        // Fill out the form and submit it
        $form = $crawler->selectButton('Save')->form();
        // Set form data as needed
        // $form['location[propertyName]'] = 'value';
        $client->submit($form);

        $this->assertResponseRedirects('/location/');
    }

    public function testShow()
    {
        $client = static::createClient();

        $client->request('GET', '/location/1'); // Assuming there's a location with ID 1

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Location Details');
    }

}
