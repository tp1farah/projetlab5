<?php

namespace App\Tests\Entity;

use App\Entity\Location;
use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class  VoitureUnitTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $voiture = new Voiture();

        $voiture->setSerie('ABC123');
        $this->assertEquals('ABC123', $voiture->getSerie());

        $voiture->setDateMise('2022-01-01');
        $this->assertEquals('2022-01-01', $voiture->getDateMise());

        $voiture->setMarche('Brand');
        $this->assertEquals('Brand', $voiture->getMarche());

        $voiture->setModele('Model X');
        $this->assertEquals('Model X', $voiture->getModele());

        $voiture->setPrixJour('50');
        $this->assertEquals('50', $voiture->getPrixJour());
    }

    public function testLocationRelationship()
    {
        $voiture = new Voiture();
        $location = new Location();

        $this->assertCount(0, $voiture->getLocation());

        $voiture->addLocation($location);

        $this->assertCount(1, $voiture->getLocation());
        $this->assertTrue($voiture->getLocation()->contains($location));
        $this->assertSame($voiture, $location->getVoiture());

        $voiture->removeLocation($location);

        $this->assertCount(0, $voiture->getLocation());
        $this->assertNull($location->getVoiture());
    }
}
