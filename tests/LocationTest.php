<?php

namespace App\Tests\Entity;

use App\Entity\Client;
use App\Entity\Location;
use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class LocationUnitTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $location = new Location();

        $location->setDateDebut('2022-01-01');
        $this->assertEquals('2022-01-01', $location->getDateDebut());

        $location->setDateRetour('2022-01-10');
        $this->assertEquals('2022-01-10', $location->getDateRetour());

        $location->setPrix('100');
        $this->assertEquals('100', $location->getPrix());
    }

    public function testVoitureRelationship()
    {
        $location = new Location();
        $voiture = new Voiture();

        $this->assertNull($location->getVoiture());

        $location->setVoiture($voiture);

        $this->assertSame($voiture, $location->getVoiture());
    }

    public function testClientRelationship()
    {
        $location = new Location();
        $client = new Client();

        $this->assertNull($location->getClient());

        $location->setClient($client);

        $this->assertSame($client, $location->getClient());
    }
}
