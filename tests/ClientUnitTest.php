<?php

namespace App\Tests\Entity;

use App\Entity\Client;
use App\Entity\Location;
use PHPUnit\Framework\TestCase;

class ClientUnitTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $client = new Client();

        $client->setCin('123ABC');
        $this->assertEquals('123ABC', $client->getCin());

        $client->setNom('John');
        $this->assertEquals('John', $client->getNom());

        $client->setPrenom('Doe');
        $this->assertEquals('Doe', $client->getPrenom());

        $client->setAdresse('123 Main St');
        $this->assertEquals('123 Main St', $client->getAdresse());
    }

    public function testLocationRelationship()
    {
        $client = new Client();
        $location = new Location();

        $this->assertCount(0, $client->getLocation());

        $client->addLocation($location);

        $this->assertCount(1, $client->getLocation());
        $this->assertTrue($client->getLocation()->contains($location));
        $this->assertSame($client, $location->getClient());

        $client->removeLocation($location);

        $this->assertCount(0, $client->getLocation());
        $this->assertNull($location->getClient());
    }
}
