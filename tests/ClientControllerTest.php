// tests/Controller/ClientControllerTest.php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ClientControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();

        $client->request('GET', '/client/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Clients');
    }

    public function testNew(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/client/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create a new client');

        $form = $crawler->selectButton('Save')->form();

        // Set form values as per your form structure
        $form['client[nom]'] = 'John';
        $form['client[prenom]'] = 'Doe';
        // ... other fields

        $client->submit($form);

        $this->assertResponseRedirects('/client/');
    }

    public function testShow(): void
    {
        $client = static::createClient();

        $client->request('GET', '/client/1'); // Replace 1 with a valid client ID

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Client Details');
    }

    //public function testEdit(): void
    //{
      //  $client = static::createClient();

       //$crawler = $client->request('GET', '/client/1/edit'); // Replace 1 with a valid client ID

        //$this->assertResponseIsSuccessful();
        //$this->assertSelectorTextContains('h1', 'Edit client');

        //$form = $crawler->selectButton('Save')->form();

        // Update form values as needed
        //$form['client[nom]'] = 'Updated Name';
        // ... other fields

        //$client->submit($form);

        //$this->assertResponseRedirects('/client/');
    //}

   // public function testDelete(): void
   // {
     //   $client = static::createClient();

        // Replace 1 with a valid client ID
       // $client->request('POST', '/client/1/delete', ['_token' => 'your_csrf_token']);

        //$this->assertResponseRedirects('/client/');
    //}
}
