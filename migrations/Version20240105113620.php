<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240105113620 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE demo');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C744045564D218E');
        $this->addSql('DROP INDEX IDX_C744045564D218E ON client');
        $this->addSql('ALTER TABLE client DROP location_id');
        $this->addSql('ALTER TABLE location ADD voiture_id INT DEFAULT NULL, ADD client_id INT DEFAULT NULL, CHANGE date_debut date_debut VARCHAR(255) DEFAULT NULL, CHANGE date_retour date_retour VARCHAR(255) DEFAULT NULL, CHANGE prix prix VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB181A8BA FOREIGN KEY (voiture_id) REFERENCES voiture (id)');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_5E9E89CB181A8BA ON location (voiture_id)');
        $this->addSql('CREATE INDEX IDX_5E9E89CB19EB6921 ON location (client_id)');
        $this->addSql('ALTER TABLE voiture DROP FOREIGN KEY FK_E9E2810FEAB5DEB');
        $this->addSql('DROP INDEX IDX_E9E2810FEAB5DEB ON voiture');
        $this->addSql('ALTER TABLE voiture ADD date_mise VARCHAR(255) DEFAULT NULL, ADD marche VARCHAR(255) DEFAULT NULL, DROP many_to_one_id, DROP date_mise_en_marche, CHANGE prix_jour prix_jour VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE demo (id INT AUTO_INCREMENT NOT NULL, demo VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE voiture ADD many_to_one_id INT DEFAULT NULL, ADD date_mise_en_marche VARCHAR(255) NOT NULL, DROP date_mise, DROP marche, CHANGE prix_jour prix_jour DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE voiture ADD CONSTRAINT FK_E9E2810FEAB5DEB FOREIGN KEY (many_to_one_id) REFERENCES location (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_E9E2810FEAB5DEB ON voiture (many_to_one_id)');
        $this->addSql('ALTER TABLE client ADD location_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C744045564D218E FOREIGN KEY (location_id) REFERENCES location (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_C744045564D218E ON client (location_id)');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CB181A8BA');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CB19EB6921');
        $this->addSql('DROP INDEX IDX_5E9E89CB181A8BA ON location');
        $this->addSql('DROP INDEX IDX_5E9E89CB19EB6921 ON location');
        $this->addSql('ALTER TABLE location DROP voiture_id, DROP client_id, CHANGE date_debut date_debut DATE DEFAULT NULL, CHANGE date_retour date_retour DATE DEFAULT NULL, CHANGE prix prix DOUBLE PRECISION DEFAULT NULL');
    }
}
